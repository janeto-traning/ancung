import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NaviBarMenuComponent } from './navi-bar-menu.component';

describe('NaviBarMenuComponent', () => {
  let component: NaviBarMenuComponent;
  let fixture: ComponentFixture<NaviBarMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NaviBarMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NaviBarMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
