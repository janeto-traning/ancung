import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrganizatonComponent } from './organizaton.component';

describe('OrganizatonComponent', () => {
  let component: OrganizatonComponent;
  let fixture: ComponentFixture<OrganizatonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrganizatonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrganizatonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
