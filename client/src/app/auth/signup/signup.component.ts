import { Component, OnInit } from '@angular/core';
import { UserServiceService } from './../../Services/user-service.service';
@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
  username: string;
  email: string;
  password: string;
  sex: string;
  telephone: string;
  constructor(
    private userSV: UserServiceService) { }

  ngOnInit() {

  }
  singUp() {
    this.userSV.sendSingup(this.username, this.email, this.password, this.sex, this.telephone).subscribe(
      res => {
        console.log(res);
      });
  }

}
