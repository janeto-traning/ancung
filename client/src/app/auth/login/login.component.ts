import { UserStoreService } from '../../Services/user-store.service';
import { UserServiceService } from './../../Services/user-service.service';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { LocalStorageService } from 'angular-2-local-storage';
import { Http } from '@angular/http';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { Subscriber } from 'rxjs/Subscriber';
import { Subject } from 'rxjs/Subject';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  email: string;
  password: string;
  id: String;
  constructor(
    private router: Router,
    private localStorage: LocalStorageService,
    private userSV: UserServiceService,
    public toastr: ToastsManager,
    private user: UserStoreService
  ) { }
  ngOnInit() {


  }
  checkLogin() {
    this.userSV.checkLogin(this.email, this.password).subscribe(res => {
      this.user.setUser(res.user);
      this.userSV.subcriber.next('ok');
      this.router.navigate(['/orderdoashboard']);
    });
  }


}
