import { UserServiceService } from './../Services/user-service.service';
import { Component, OnInit } from '@angular/core';
import { UserStoreService } from '../Services/user-store.service';
import { IUsers } from '../models/users';
@Component({
  selector: 'app-settingprofile',
  templateUrl: './settingprofile.component.html',
  styleUrls: ['./settingprofile.component.scss']
})
export class SettingprofileComponent implements OnInit {
  user: IUsers;
  a: Number = 0;
  username: string;
  email: string;
  sex: string;
  telephone: string;
  password: string;
  constructor(private User: UserStoreService, private userSV: UserServiceService) { }

  ngOnInit() {
    this.User.getUser().subscribe((user) => {
      this.user = user;
      console.log(user);
    });


  }
  changeprofile() {
    this.userSV.changeprofile(this.username, this.email, this.password, this.sex, this.telephone).subscribe(
      res => {
        console.log(res);
      }
    );
  }

}
