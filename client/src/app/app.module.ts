import { GruopServiceService } from './Services/gruop/gruop-service.service';
import { LoginComponent } from './auth/login/login.component';
import { UserServiceService } from './Services/user-service.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Component } from '@angular/core';
import { AppComponent } from './app.component';
import { DoashboardComponent } from './DoashBoard/doashboard/doashboard.component';
import { LocalStorageModule } from 'angular-2-local-storage';
import { LoginserviceService } from './Services/LoginService/loginservice.service';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { NaviBarMenuComponent } from './navi-bar-menu/navi-bar-menu.component';
import { SignupComponent } from './auth/signup/signup.component';
import { OderFoodDoashboardComponent } from './oder-food-doashboard/oder-food-doashboard.component';
import { NgModel } from '@angular/forms/src/directives/ng_model';
import { RouterModule, CanActivate } from '@angular/router';
import { AgmCoreModule } from '@agm/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastModule } from 'ng2-toastr/ng2-toastr';
import { ToastrService } from 'ngx-toastr';
import { CreatemenuComponent } from './oder-food-doashboard/createmenu/createmenu.component';
import { ListmenuComponent } from './oder-food-doashboard/listmenu/listmenu.component';
import { OrganizatonComponent } from './organizaton/organizaton.component';
import { ListOrganizationComponent } from './organizaton/list-organization/list-organization.component';
import { SettingprofileComponent } from './settingprofile/settingprofile.component';
import { SettingbillingComponent } from './settingbilling/settingbilling.component';
<<<<<<< Updated upstream
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule, MatCheckboxModule } from '@angular/material';
import { UserStoreService } from './Services/user-store.service';
import { GruopComponent } from './gruop/gruop.component';
import { LeftComponent } from './oder-food-doashboard/left/left.component';
import { CreategroupComponent } from './gruop/creategroup/creategroup.component';
<<<<<<< HEAD
import { GruopStoreService } from './Services/gruop/gruop-store.service';
import { GruopdetailComponent } from './gruop/gruopdetail/gruopdetail.component';
import { CreateRestaurantComponent } from './oder-food-doashboard/create-restaurant/create-restaurant.component';
import { RestaurantService } from './Services/Restaurant/restaurant.service';
=======
=======
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {MatButtonModule, MatCheckboxModule} from '@angular/material';
import { PagenotfoundComponent } from './pagenotfound/pagenotfound.component';
import { DialogComponent } from './dialog/dialog.component';
>>>>>>> Stashed changes
>>>>>>> db6b47a230b0fb90ba7dcf27bc4b614fefeab40f

@NgModule({
  declarations: [
    AppComponent,
    DoashboardComponent,
    NaviBarMenuComponent,
    SignupComponent,
    LoginComponent,
    OderFoodDoashboardComponent,
    CreatemenuComponent,
    ListmenuComponent,
    OrganizatonComponent,
    ListOrganizationComponent,
    SettingprofileComponent,
    SettingbillingComponent,
<<<<<<< Updated upstream
    GruopComponent,
    LeftComponent,
<<<<<<< HEAD
    CreategroupComponent,
    GruopdetailComponent,
    CreateRestaurantComponent
=======
    CreategroupComponent
=======
    PagenotfoundComponent,
    DialogComponent
>>>>>>> Stashed changes
>>>>>>> db6b47a230b0fb90ba7dcf27bc4b614fefeab40f
  ],
  imports: [
    BrowserModule,
    LocalStorageModule.withConfig({
      prefix: 'my-app',
      storageType: 'localStorage'
<<<<<<< Updated upstream
    }),
    BrowserModule,
    FormsModule,
    RouterModule.forRoot([
      { path: '', component: DoashboardComponent },
      {
        canActivate: [LoginserviceService],
        path: 'orderdoashboard',
        component: OderFoodDoashboardComponent,
        children: [
          { path: 'group', component: GruopComponent },
          { path: 'group/:id', component: GruopdetailComponent },
          { path: 'creategruop', component: CreategroupComponent },
          { path: 'settingprofile', component: SettingprofileComponent },
          { path: 'createmenu', component: CreatemenuComponent },
          { path: 'createRestaurant', component: CreateRestaurantComponent }

        ]
      },
      {
        path: 'login', component: LoginComponent,
      },
      {
        path: 'signup', component: SignupComponent,
      },
      {
        path: 'organization', component: OrganizatonComponent,
      },
      {
        path: 'profile', component: SettingprofileComponent,
      }
    ]),
    HttpModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyBBnwLr1x3XC6YbJXlSHQAkR1xZ88jqNWE',
      libraries: ['places']
    }),
    BrowserAnimationsModule,
    ToastModule.forRoot(),
    BrowserAnimationsModule,
    NoopAnimationsModule,
    MatButtonModule, MatCheckboxModule

=======
  }),
  BrowserModule,
  FormsModule,
  RouterModule.forRoot([
    {
      path: '', component : DoashboardComponent
    },
    {
      canActivate: [LoginserviceService],
      path: 'orderdoashboard', component: OderFoodDoashboardComponent , 
       
    },
    {
      canActivate: [LoginserviceService],
      path: 'organization', component: OrganizatonComponent
    },
    {
      path: 'login', component : LoginComponent,
    },
    {
      path: 'signup', component: SignupComponent,
    },
    {
      path: '**', component: PagenotfoundComponent
    }
  ]),
  HttpModule,
  AgmCoreModule.forRoot({
    apiKey: "AIzaSyBBnwLr1x3XC6YbJXlSHQAkR1xZ88jqNWE",
    libraries: ["places"]
  }),
  BrowserAnimationsModule,
  ToastModule.forRoot(),
  BrowserAnimationsModule,
  NoopAnimationsModule,
  MatButtonModule, MatCheckboxModule
>>>>>>> Stashed changes
  ],
  providers: [UserServiceService, LoginserviceService, UserStoreService, GruopServiceService, GruopStoreService, RestaurantService],
  bootstrap: [AppComponent]
})
export class AppModule { }
