import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GruopdetailComponent } from './gruopdetail.component';

describe('GruopdetailComponent', () => {
  let component: GruopdetailComponent;
  let fixture: ComponentFixture<GruopdetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GruopdetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GruopdetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
