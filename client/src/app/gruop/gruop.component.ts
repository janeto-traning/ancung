import { GruopStoreService } from './../Services/gruop/gruop-store.service';
import { UserServiceService } from './../Services/user-service.service';
import { UserStoreService } from './../Services/user-store.service';
import { Component, OnInit } from '@angular/core';
import { IGruop } from '../models';

@Component({
  selector: 'app-gruop',
  templateUrl: './gruop.component.html',
  styleUrls: ['./gruop.component.scss']
})
export class GruopComponent implements OnInit {
  groups: IGruop[] = [];
  id: String;
  constructor(private user: UserStoreService, private usersv: UserServiceService) { }

  ngOnInit() {
    this.user.getUser().subscribe((user) => {
      this.id = user._id;
    });
    this.usersv.getUser(this.id).subscribe((res) => {
      this.groups = res.user.group;
      console.log(this.groups);
    });
  }
}
