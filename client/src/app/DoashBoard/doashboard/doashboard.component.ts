import { Component, OnInit } from '@angular/core';
import { MapsAPILoader } from '@agm/core';

@Component({
  selector: 'app-doashboard',
  templateUrl: './doashboard.component.html',
  styleUrls: ['./doashboard.component.scss']
})
export class DoashboardComponent implements OnInit {
  public latitude: number;
  public longitude: number;
  public zoom: number;
  constructor() { }

  ngOnInit() {
    this.setCurrentPosition();

  }
  private setCurrentPosition() {
    if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.latitude = position.coords.latitude;
        this.longitude = position.coords.longitude;
        this.zoom = 15;
      });
    }
  }
}
