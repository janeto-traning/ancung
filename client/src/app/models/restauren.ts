import { IFood } from './food';
export interface IRestauren {
    name: String;
    address: String;
    phone?: Number;
    hinhanh?: String;
    foods: IFood[];

}
