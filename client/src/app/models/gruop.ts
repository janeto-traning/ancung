import { IUsers } from './users';
import { IOrder } from './order';
export interface IGruop {
    name: String;
    creator: IUsers;
    users: [IUsers];
    orders: [IOrder];

}
