export interface IFood {
    name: String;
    price: Number;
    image?: String;
}
