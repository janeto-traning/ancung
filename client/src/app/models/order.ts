import { IUsers } from './users';
import { IGruop } from './gruop';
import { IRestauren } from './restauren';
import { Time } from '@angular/common';
export interface IOrder {
    creatorOrder: [IUsers];
    group: IGruop;
    datetime: Date;
    timeopen: Time;
    timeclose: Time;
    restaurant: [ IRestauren ];
    picklist: [{
        name: {
            type: String,
            default: 'unknown'
        },
        food: {
            type: String,
            default: 'unknown'
        },
        price: {
            type: String,
            default: 'unknown'
        }
    }];

}
