import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OderFoodDoashboardComponent } from './oder-food-doashboard.component';

describe('OderFoodDoashboardComponent', () => {
  let component: OderFoodDoashboardComponent;
  let fixture: ComponentFixture<OderFoodDoashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OderFoodDoashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OderFoodDoashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
