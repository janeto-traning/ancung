import { RestaurantService } from './../../Services/Restaurant/restaurant.service';
import { IFood } from './../../models/food';
import { IRestauren } from './../../models/restauren';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-create-restaurant',
  templateUrl: './create-restaurant.component.html',
  styleUrls: ['./create-restaurant.component.scss']
})
export class CreateRestaurantComponent implements OnInit {
  myRestaurant: IRestauren = {
    name: '',
    address: '',
    foods: []
  };
  myFood: IFood = {
    name: '',
    price: 0
  };
  constructor(private restaurantService: RestaurantService) { }

  ngOnInit() {
  }
  CreateRestaurant() {
    // this.restaurantService.createRestaurant(this.name, this.diachi, this.sodienthoai, this.foods).subscribe(res =>
    //   console.log(this.name)
    // );

    this.restaurantService.createRestaurant(this.myRestaurant).subscribe(res =>
      console.log(res)
    );
  }
  CreateFood() {
    // this.food.name = this.namefood;
    // this.food.price = this.moneyfood;
    // this.foods.push(this.food);
    // console.log(this.food);
    // console.log(this.foods);
    // kiểm tra rỗng cac thứ...

    this.myRestaurant.foods.push(this.myFood);
    this.myFood = {
      name: '',
      price: 0
    };
  }

}
