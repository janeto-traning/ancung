import { Component, OnInit } from '@angular/core';
import { UserStoreService } from '../../Services/user-store.service';

@Component({
  selector: 'app-left',
  templateUrl: './left.component.html',
  styleUrls: ['./left.component.scss']
})
export class LeftComponent implements OnInit {
  name: String;
  constructor(private User: UserStoreService) { }

  ngOnInit() {
    this.User.getUser().subscribe((user) => {
        this.name = user.username;
    });
  }
}
