import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SettingbillingComponent } from './settingbilling.component';

describe('SettingbillingComponent', () => {
  let component: SettingbillingComponent;
  let fixture: ComponentFixture<SettingbillingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SettingbillingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SettingbillingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
