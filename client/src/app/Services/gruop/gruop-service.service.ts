import { IUsers, IGruop } from './../../models';

import { IOrder } from './../../models';
import { Http } from '@angular/http';
import { Injectable } from '@angular/core';

@Injectable()
export class GruopServiceService {
  url: string;
  id: String;
  constructor(private http: Http) { }
  creategruop(name: String, creater: IUsers, user: IUsers[], order: IOrder[]) {
    this.id = creater._id;
    this.url = 'http://localhost:8080/apiGroup/' + this.id;
    return this.http.post(this.url, { name: name, creator: creater, user: user, order: order }).map(data => {
      return data.json();
    }).catch(error => {
      console.log(error);
      return error;
    });
  }

  // findgroupyId(id: number): Observable<IGruop>  {
  //   return this.getBooks().map(books => {
  //     return books.find(book => {
  //       return book.id === id;
  //     });
  //   });
  // }

}
