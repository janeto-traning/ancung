import { TestBed, inject } from '@angular/core/testing';

import { GruopStoreService } from './gruop-store.service';

describe('GruopStoreService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GruopStoreService]
    });
  });

  it('should be created', inject([GruopStoreService], (service: GruopStoreService) => {
    expect(service).toBeTruthy();
  }));
});
