import { TestBed, inject } from '@angular/core/testing';

import { GruopServiceService } from './gruop-service.service';

describe('GruopServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GruopServiceService]
    });
  });

  it('should be created', inject([GruopServiceService], (service: GruopServiceService) => {
    expect(service).toBeTruthy();
  }));
});
