import { IUsers } from './../models/users';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class UserStoreService {
  private user = new BehaviorSubject<IUsers>(null);
  constructor() { }
  setUser(user: IUsers) {
    this.user.next(user);
    console.log(user._id);
  }
  getUser() {
    return this.user.asObservable();
  }

}
