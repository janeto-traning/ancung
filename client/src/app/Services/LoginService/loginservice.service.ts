import { UserStoreService } from './../user-store.service';
import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { LocalStorageService } from 'angular-2-local-storage';
@Injectable()
export class LoginserviceService {

  constructor(private localStorage: LocalStorageService, private router: Router, private user: UserStoreService ) { }
  canActivate() {
    const login = this.user.getUser().subscribe();
    if (!login) {
      alert('Mật khẩu không đúng hoặc chưa đăng ký');
      this.router.navigate(['/login']);
    }
    return login ? true : false;
  }
  }


