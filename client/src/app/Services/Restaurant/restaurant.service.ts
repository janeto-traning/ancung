import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import { IRestauren } from '../../models';

@Injectable()
export class RestaurantService {

  constructor(private http: Http) { }
  createRestaurant(restaurant: IRestauren) {
    return this.http.post('http://localhost:8080/apiRestaurant', restaurant).map(data => {
      console.log(data.json);
      return data.json();
    }).catch(err => {
      console.log(err);
      return   err;
    });
  }

}
