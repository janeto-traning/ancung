var User = require('../models/user.model');
var crypto = require('crypto');
var secret = 'harrypotter';
var path = require('path');
var constants = require('../constants');
var mail = require('../utils/mail');

module.exports = {
    getUser: getUser,
    getUsers: getUsers,
    createUser: createUser,
    updateUser: updateUser,
    deleteUser: deleteUser,
    uploadAvatar: uploadAvatar
}
function getUser(id) {
    return User.findById(id, { _id: 0, password: 0 })
    .populate('group')
        .then((user) => {
            return Promise.resolve(user);
        })
        .catch((err) => {
            return Promise.reject(err);
        })
}
function uploadAvatar(userId, file) {
    //find user to upload avatar
    console.log('testing here')
    return User.findOne({ _id: userId })
        .then(function (user) {
            if (user) {
                return new Promise(function (resolve, reject) {
                    //move to avatar folder
                    file.mv(path.join(__dirname, '../public/avatar/avatar_' + user._id + '.png'), function (err) {
                        if (err)
                            return reject(err);

                        //update current user with new avatar path
                        return User.update({ _id: userId }, { $set: { image: 'avatar_' + user._id + '.png' } })
                            .then(function (data) {
                                return resolve(data);
                            })
                            .catch(function (err) {
                                return reject(err);
                            })
                    });
                });

            } else {
                return Promise.reject({
                    message: "Not Found",
                    statusCode: 404
                });
            }
        })
        .catch(function (err) {
            return Promise.reject(err);
        })
}
function deleteUser(id) {
    return User.findOneAndRemove(id)
        .then(() => {
            return Promise.resolve();
        })
        .catch((err) => {
            return Promise.reject(err);
        })

}
function updateUser(user) {
    return User.findByIdAndUpdate(user._id, user)
        .then(function (user) {
            return Promise.resolve(user);
        })
        .catch(function (err) {
            return Promise.reject(err);
        })
}
function getUsers() {
    return User.find({}, { _id: 0, password: 0 })
        .then(function (users) {
            console.log(users)
            return Promise.resolve(users);
        })
        .catch(function (err) {
            return Promise.reject(err);
        })
}

function createUser(newUser) {
    return User.find({ email: newUser.email })
        .then(function (foundUsers) {
            if (foundUsers.length > 0) {
                return Promise.reject({
                    statusCode: 400,
                    message: 'Email is existed'

                });
            } else {
                var hash = crypto.createHmac('sha256', secret)
                    .update(newUser.password)
                    .digest('hex');
                newUser.password = hash;
                var user = new User(newUser);
                return user.save()
                    .then(function (user) {
                        console.log(user);
                        return mail.sendMail('', user.email, 'New user registration', '<h1>Wellcome to the APP</h1>')
                            .then(function (res) {
                                return Promise.resolve(res);
                            })
                            .catch(function (err) {
                                return Promise.reject(err);
                            })
                    })
                    .catch(err => {
                        console.log(err);
                        return Promise.reject(err);
                    })
            }
        })
        .catch(function (err) {
            return Promise.reject(err);
        })
}

