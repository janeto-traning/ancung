var Restaurant = require('../models/restaurant.model');

module.exports = {
    createRestaurant: createRestaurant,
    getRestaurants:getRestaurants,
    updateRestaurant:updateRestaurant,
     deleteRestaurant:deleteRestaurant
}
function deleteRestaurant(id){
    console.log('testing here!')
    return Restaurant.findByIdAndRemove(id)
        .then(()=>{
            return Promise.resolve();
        })
        .catch((err)=>{
            return Promise.reject(err);
        })
}
function updateRestaurant(newRestaurant){
    return Restaurant.findByIdAndUpdate({_id:newRestaurant._id},newRestaurant)
        .then((restaurant)=>{
            return Promise.resolve(restaurant);
        })
        .catch((err)=>{ 
            return Promise.reject(err);
        })
}

function getRestaurants(){
    return Restaurant.find()
        .then((restaurants)=>{
            return Promise.resolve(restaurants);
        })
        .catch((err)=>{
            return Promise.reject(err);
        })
}
function createRestaurant(restaurant) {
    return Restaurant.find({ address: restaurant.address })
        .then((foundRestaurant) => {
            if (foundRestaurant.length > 0) {
                return Promise.reject({
                    statusCode: 400,
                    message: 'restaurant is existed'
                })
            } else {
                var newRestaurant = new Restaurant(restaurant);
                return newRestaurant.save()
                    .then((newRestaurant) => {
                        return Promise.resolve(newRestaurant);
                    })
                    .catch((err) => {
                        return Promise.reject(err);
                    })
            }
        })
        .catch((err)=>{
            return Promise.reject(err);
        })


}