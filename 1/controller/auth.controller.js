    var User = require('../models/user.model');
    var crypto = require('crypto');
    var jwt = require('../utils/jwt');
    var secret = 'harrypotter';

    module.exports = {
        login: login,
        checkAuth: checkAuth
    }
    function login(email, password) {
        var hash = crypto.createHmac('sha256', secret)
            .update(password)
            .digest('hex');
        return User.findOne({
            email: email,
            password : hash
            
        },{password:0,_v:0})
            .then(function (user) {
               console.log(user)
                if (user) {
                    return new Promise(function (resolve, reject) {
                        jwt.sign({
                            email: user.email
                        }, function (err, token) {
                            if (err) {
                              return  reject({
                                    statusCode: 400,
                                    message: err.message
                                });
                            } else {
                               return resolve({token,user});
                            }
                        })
                    });
    
                } else {
                    return Promise.reject({
                        statusCode: 400,
                        message: 'Email or password is incorrect'
                    });
                }
            })
            .catch(function (err) {
                return Promise.reject(err);
            })
    }
    

function checkAuth(user) {
    return User.findOne(user)
        .then(function (foundUser) {
            if (foundUser) {
                return Promise.resolve(foundUser);
            } else {
                return Promise.reject({
                    message: 'Not Found'
                });
            }
        })
        .catch(function (err) {
            return Promise.reject(err);
        })
}