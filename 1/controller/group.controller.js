var User = require('../models/user.model');
var Group = require('../models/group.model');

module.exports = {
    getGroup: getGroup,
    createGroup: createGroup,
    getGroups: getGroups,
    updateGroup: updateGroup,
    updateGroupUser: updateGroupUser,
    deleteGroup: deleteGroup
}
function updateGroupUser(idGroup, idUser) {

    return Group.findOne({ _id: idGroup })
        .then((group) => {

            group.update(
                { _id: group_id }, { $addToSet: { users: { $each: [idUser]}}})
                console.log(group)
            return Group.findByIdAndUpdate(group._id, group)
                .catch((err) => {
                    return Promise.reject(err);
                })
        })
        .catch((err) => {
            return Promise.reject(err);
        })

}
function getGroup(id) {
    return Group.findById(id)
        .populate('users')
        .populate('creator')
        .then((group) => {
            return Promise.resolve(group);
            console.log(group);
        })
        .catch((err) => {
            return Promise.reject(err);
        })
}
function deleteGroup(id) {
    return Group.findByIdAndRemove(id)
        .then(() => {
            return Promise.resolve({
                message: 'success'
            })
        })
        .catch((err) => {
            return Promise.reject(err);
        })
}
function updateGroup(group) {

    return Group.findByIdAndUpdate(group._id, group)
        .then((updateGroup) => {
            console.log(updateGroup);
            return Promise.resolve(updateGroup);
        })
        .catch((err) => {
            return Promise.reject(err);
        })
}
function getGroups() {
    return Group.find()
        .then((Group) => {
            return Promise.resolve(Group);
        })
        .catch((err) => {
            return Promise.reject(err);
        })
}
function createGroup(newGroup,userId) {
    return Group.find({ name: newGroup.name })
        .then((foundGroup) => {
            if (foundGroup.length > 0) {
                return Promise.reject({
                    status: 400,
                    message: 'Group is existed'
                })
            }
            else {
                return newGroup.save()
                    .then((group) => {
                       var id = group._id;
                        User.findOne({ _id: userId }, (err, user) => {
                            if (err) {
                              next(err);
                            }
                            var group1 = user.group;
                            console.log(id+'sasas')
                            group1.push(id);
                            user.group = group1;
                            user.save((err) => {
                              if (err) {
                                next(err);
                              }
                            });
                          })
                        return Promise.resolve(group);
                        
                    })
                    .then((err) => {
                        return Promise.reject(err);
                    })
            }
        })
}