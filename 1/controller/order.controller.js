var Order = require('../models/order.model');
var User = require('../models/user.model');
var Group = require('../models/group.model');

module.exports = {
    createOrder: createOrder,
    getOrder:getOrder,
    updateOrder:updateOrder,
  //  deleteOrder:deleteRestaurant
}
function updateOrder(updateOrder){
    return Order.findByIdAndUpdate({_id:updateOrder._id},updateOrder)
        .then((order)=>{
            return Promise.resolve(order);
        })
        .catch((err)=>{
            return Promise.reject(err);
        })
}
function getOrder(){
    return Order.find()
        .then((groups)=>{
            return Promise.resolve(groups);
        })
        .catch((err)=>{
            return Promise.reject(err);
        })
}
function createOrder (newOrder)
{
    return newOrder.save()
        .then((order)=>{
            return Promise.resolve(order)
        })
        .catch((err)=>{
            return Promise.reject(err);
        })
}