var router = require('express').Router();
var Restaurant = require('../models/restaurant.model');
var RestaurantController = require('../controller/restaurant.controller');

router.post('/uploadi')
router.post('/',createRestaurant);
router.get('/',getRestaurant);
router.put('/:id',updateRestaurant);
router.delete('/:id',deleteRestaurant);
module.exports = router;

function deleteRestaurant(req,res,next){
    var id_restaurant = req.params.id;
    RestaurantController.deleteRestaurant(id_restaurant)
        .then(()=>{
            res.json({message:'success'})
        })
        .catch((err)=>{
            res.json(err);
        })
}

function updateRestaurant(req,res,next){
    var id_restaurant = req.params.id;
    var newRestaurant = req.body;
    newRestaurant._id = id_restaurant;
    RestaurantController.updateRestaurant(newRestaurant)
        .then(()=>{
            return res.json({message:'success'})
        })
        .catch((err)=>{
            return res.json({message:'error!'})
        })
}

function getRestaurant(req,res,next){
    RestaurantController.getRestaurants()
        .then((restaurants)=>{
            return res.json(restaurants);
        })
        .catch ((err)=>{
            return resizeBy.send(err);
        })
}
function createRestaurant (req,res,next){
    var newRestaurant = new Restaurant();
    newRestaurant.name = req.body.name;
    newRestaurant.address =req.body.address;
    newRestaurant.foods = req.body.foods;
    for(var i = 0 ; i<req.body.foods.length;i++){
        newRestaurant.foods[i].name = req.body.foods[i].name;
        newRestaurant.foods[i].price = req.body.foods[i].price;
        
    }
    if(newRestaurant.name == null||newRestaurant.name==''||newRestaurant.address==null||newRestaurant.address==''){
        res.send('please provide infimation! ')
    }
    else {
        RestaurantController.createRestaurant(newRestaurant)
            .then((restaurant)=>{
                return res.json(restaurant)
            })
            .catch((err)=>{
                return res.send(err);
            })
    }
    
}