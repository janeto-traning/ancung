var router = require('express').Router();
var Group = require('../models/group.model');
var User = require('../models/user.model');
var GroupController = require('../controller/group.controller');

router.get('/:id', getGroup);
router.get('/', getGroups);
router.post('/:userId', createGroup);
router.put('/:id', updateGroup);
router.post('/:id/:idUser', updateGroupUser);
router.delete('/:id', deleteGroup);
//huy dep zai
module.exports = router;
function updateGroupUser(req, res, next) {
  var id = req.params.id;
  var idUser = req.params.idUser;
  var updateGroup = new Group(req.body);
  updateGroup._id = id;
  User.findOne({ _id: idUser }, (err, user) => {
    if (err) {
      next(err);
    }
    var group = user.group;
    group.push(id);
    user.group = group;
    user.save((err) => {
      if (err) {
        next(err);
      }
    });
  })
  GroupController.updateGroupUser(updateGroup._id,idUser)
    .then(() => {
      return res.send({message:'success!'});
    })
    .catch((err) => {
      return res.json(err);
    })
}
function getGroup(req, res, next) {
  var id = req.params.id;
  GroupController.getGroup(id)
    .then((group) => {
      return res.send(group);
    })
    .catch((err) => {
      return res.json(err);
    })
}

function deleteGroup(req, res, next) {
  var id = req.params.id;
  GroupController.deleteGroup()
    .then(() => {
      return res.json({
        message: 'success'
      })
    })
    .catch((err) => {
      return res.json(err);
    })
}

function updateGroup(req, res, next) {
  var id = req.params.id;
  var updateGroup = new Group(req.body);
  updateGroup._id = id;

  GroupController.updateGroup(updateGroup)
    .then((group) => {
      return res.send(group);
    })
    .catch((err) => {
      return res.json(err);
    })
}

function getGroups(req, res, next) {
  GroupController.getGroups()
    .then((group) => {
      return res.send(group);
    })
    .catch((err) => {
      return res.json(err);
    })
}

function createGroup(req, res, next) {
  var userId = req.params.userId;
  var newGroup = new Group(req.body);
  GroupController.createGroup(newGroup,userId)
    .then((group) => {
      return res.json({ message: 'success' })
    })
    .catch((err) => {
      res.send(err);
    })


  // newGroup.name = req.body.name;
  // newGroup.boss = new User({
  //   _id: req.body.boss._id
  // });
  // console.log( newGroup.users.length);

  // User.find({ _id: id })
  //   .then((user) => {
  //     console.log(user);
  //     newGroup.boss.push(user);
  //     console.log(id);


  //   })
  // for(var i = 0 ;i<=newGroup.createUser.length;i++){
  //     newGroup.createUser[i].name = req.body.createUser[i].name;
  // }
  // console.log(newGroup.createUser.length);
  //console.log(newGroup+'ddd');

}


