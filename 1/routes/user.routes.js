var router = require('express').Router();
var User = require('../models/user.model');
var UserController = require('../controller/user.controller');
var auth = require('../middle-ware/auth');


// router.get('/', getUsers);
router.get('/:id',getUser)
router.get('/', getUsers);
router.post('/', createUser);
router.put('/:id', updateUser);
router.delete('/:id', deleteUser);
router.post('/avatar', auth.auth(), uploadAvatar);

module.exports = router;

function getUser(req,res,next){
    var id = req.params.id;
    UserController.getUser(id)
        .then((user)=>{
            return res.json({user});
        })
        .catch((err)=>{
            next(err);
        })
}
function uploadAvatar(req, res, next) {
   
    if (!req.files){
        return res.status(400).send('No files were uploaded.');
      }
      let file = req.files.file;
      console.log(file+'sadsadsa');

    UserController.uploadAvatar(req.user._id, file)
        
        .then(function (avatar) {
            res.send({
                avatar: avatar
            })
        })
        .catch(function (err) {
            next(err);
        })
}
function deleteUser(req, res, next) {
    var id = req.params.id;
    UserController.deleteUser(id)
        .then(() => {
            return res.json({ message: 'success!' })
        })
        .catch((err) => {
            return res.json({
                message: 'error!'
            });
        })
}
function updateUser(req, res, next) {
    var id = req.params.id;
    var user = req.body;
    user._id = id;
    UserController.updateUser(user)
        .then((user) => {
            return res.json(user)
        })
        .catch((err) => {
            return res.json({
                message: error
            })
        })
}

function getUsers(req, res, next) {
    UserController.getUsers()
        .then((users) => {
            res.json(users);
        })
        .catch((err) => {
            //return next(err) // no qua middle ware 
            next(err);
        })
}

function createUser(req, res, next) {

    var newUser = new User(req.body);
    // newUser.username = req.body.username;
    // newUser.password = req.body.password;
    // newUser.email = req.body.email;
    // newUser.phone = req.body.phone;
    // newUser.sex = req.body.sex;

    // var user1 = new User();
    //   var user1 = req.body;
    // req.body.testline = 'abc'
    console.log(newUser);

    if (!newUser.email) {
        next({
            statusCode: 400,
            message: 'email is required'
        })
    }
    else if (!newUser.password) {
        next({
            statusCode: 400,
            message: 'password is required'
        })
    } else if (!newUser.username) {
        next({
            statusCode: 400,
            message: 'username is required'
        })
    } else if (!newUser.phone) {
        next({
            statusCode: 400,
            message: 'phone is required'
        })
    } else {
        UserController.createUser(newUser)
            .then((user) => {
                return res.json(user);
            })
            .catch((err) => {
                //return res.send(err);
                 return next(err);
            })
    }
}
