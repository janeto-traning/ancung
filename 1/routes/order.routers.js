var router = require('express').Router();
var User = require('../models/user.model');
var Order = require('../models/order.model');
var OrderController = require('../controller/order.controller');


router.get('/',getOrders);
router.post('/',createOrder);
router.put('/:id',updateOrder);
//router.delete('/:id',updateOrder);

module.exports = router;

function updateOrder(req,res){
    var id = req.params.id;
    var order = new Order(req.body);
    order._id = id;
    OrderController.updateOrder(order)
        .then(()=>{
            return res.json({message:'success'})
        })
        .catch(()=>{
            return res.send(err);
        })
}
function getOrders(req,res,next)
{
    OrderController.getOrder()
        .then((orders)=>{
            return res.send(orders);
        })
        .catch((err)=>{
            return res.send(err);
        })
}
function createOrder(req,res,next)
{
    console.log('new');
    var newOrder = new Order(req.body);
    
    OrderController.createOrder(newOrder)
        .then((order)=>{
            return res.json({message:'success'})
        })
        .catch((err)=>{
            return res.json(err);
        })
}