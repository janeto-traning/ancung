var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var userSchema = new Schema({
    username:{
        type: String,
        required:true
    },
    password: {
        type: String,
        required:true
    },
    email: {
        type: String,
        required:true
    },
    phone: {
        type: String,
        required:true
    },
    sex :{
        type: Boolean,
        required:true
    },
    image:{
        type: String,
        default:'unknown'
    },
    group: [{
        type: Schema.Types.ObjectId,
        ref : 'group'
    }],
    oldorders:[{
        type: Schema.Types.ObjectId,
        ref : 'order'
    }]

})
var user = mongoose.model('user',userSchema);
module.exports = user;
