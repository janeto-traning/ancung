var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var restaurantSchema = new Schema({
    name: String,
    address: String,
    foods: [{
        name: {
            type: String,
            required: true
        }, 
        price: {
            type: Number,
            required: true
        },
        image:{
            type: String,
            default:'unknown'
        }
    }]

    

})



var restaurant = mongoose.model('restaurant', restaurantSchema);
module.exports = restaurant;