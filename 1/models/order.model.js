var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var orderSchema = new Schema({
    creatorOrder: [{
        type: Schema.Types.ObjectId,
        ref: 'user'
    }],
    group:{
        type: Schema.Types.ObjectId,
        ref : 'group'
    },
    datetime:{
        type:Date,
        required: true
    },
    timeopen:{
       type: Date,
       required: true
    },
    timeclose:{
        type:Date,
        required:true
    },
    restaurant:[{
        type:Schema.Types.ObjectId,
        ref:'restaurat'
    }],
    picklist:[{
        name:{
            type: String,
            default:'unknown'
        } ,
        food:{
            type:String,
            default:'unknown'
        },
        price:{
            type:String,
            default:'unknown'
        }
    }]
})

var order = mongoose.model('order',orderSchema);
module.exports = order;