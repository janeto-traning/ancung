var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var groupSchema = new Schema({
    name:{
        type: String,
        required:true
    },
    creator:{
        type: Schema.Types.ObjectId,
        ref : 'user'
    },
    users:[{
        type: Schema.Types.ObjectId,
        ref : 'users'
    }],
    orders:[{
        type: Schema.Types.ObjectId,
        ref : 'order'
    }]
})


var group = mongoose.model('group',groupSchema);
module.exports = group;