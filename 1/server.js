var express         = require('express');
var app             = express();
var mongoose        = require('mongoose');
var bodyParser      = require('body-parser');
var userRoutes      = require('./routes/user.routes');
var restaurantRoutes= require('./routes/restaurant.routes');
var groupRoutes     = require('./routes/group.routers');
var orderRoutes     =require('./routes/order.routers');
var authRoutes      =require('./routes/auth.routers');
var errorHandler = require('./middle-ware/error-handler');
const fileUpload = require('express-fileupload');
var constants = require('./constants');

var allowCrossDomain = function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type, x-access-token');

    next();
};

app.use(allowCrossDomain);


app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(fileUpload());

app.use('/apiLogin',authRoutes);
app.use('/apiOrder',orderRoutes);
app.use('/apiUser',userRoutes);
app.use('/apiRestaurant',restaurantRoutes);
app.use('/apiGroup',groupRoutes);














mongoose.connect('mongodb://localhost:27017/Ancung',(err)=>{
    if(err){
        console.log('not connect to the database');
    } else {
        console.log('Suucessfully connected to MongoDB')
    }
})

app.use(errorHandler.errorHandler());



app.listen(8080,(err)=>{
    console.log('server running 8080!!')
});